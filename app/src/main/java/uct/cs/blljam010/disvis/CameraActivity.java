package uct.cs.blljam010.disvis;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class CameraActivity extends ActionBarActivity implements CameraBridgeViewBase.CvCameraViewListener2, View.OnTouchListener {


    // Preferences
    private SharedPreferences sharedPrefs;

    private CustomCameraView mOpenCvCameraView;

    private Context context;

    // controls
    private Button btnCapture;
    private Button btnStartDetection;
    private SeekBar zoomBar;

    // attributes
    private boolean detection = false;
    private Camera.Size resolution;
    private double averageRadius;
    private ArrayList<Point> wellLocations;

    // detection parameters
    private int plateSize;
    private int minRadius, maxRadius;
    private double dp, minDist, param1, param2;

    // static initialization of openCV
    static
    {
        System.loadLibrary("opencv_java");
        if (!OpenCVLoader.initDebug()) {
            Log.e("opencvstatic", "OpenCV initialization failed - cameraview");
        } else
        {
            Log.i("opencvstatic", "OpenCV initialization successful - cameraview");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // create camera
        mOpenCvCameraView = (CustomCameraView) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setFocusable(true);
        mOpenCvCameraView.setOnTouchListener(this);

        // fix landscape orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        context = getApplicationContext();

        // setup controls
        btnCapture = (Button)findViewById(R.id.btnCapture);
        btnStartDetection = (Button)findViewById(R.id.btnStartDetection);
        btnCapture.setEnabled(false);
        zoomBar = (SeekBar)findViewById(R.id.zoomBar);

        zoomBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                setZoom(progressValue);
                Log.d("TASK", "Progress Bar: "+progressValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }


    public void setZoom(int n)
    {
        mOpenCvCameraView.setZoom(n);
    }

    public void activateDetection(View v)
    {
        detection = !detection;
        btnStartDetection.setText(detection ? "Stop Detection" : "Start Detection");
        btnCapture.setEnabled(detection);
    }
    // capture the microplate
    public void capture(View v)
    {
        resolution = mOpenCvCameraView.getResolution();
        Log.v("RESOLUTION", resolution.width+", "+resolution.height);
        // (1) get today's date
        Date today = Calendar.getInstance().getTime();

        // (2) create a date "formatter" (the date format we want)
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss", Locale.ENGLISH);

        // (3) create a new String using the date format we want
        String fileName = formatter.format(today);
        Log.e("CAMERA_AVG", averageRadius+"");
        mOpenCvCameraView.takePicture(fileName, context, wellLocations, averageRadius);

    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("opencv", "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        try {
            setZoom(zoomBar.getProgress());
        } catch (Exception e){e.printStackTrace();}
    }

    public void onCameraViewStopped()
    {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat mat = inputFrame.rgba();

        if (detection)
        {

            // create greyscale mat
            Mat grayMat = new Mat(mat.cols(), mat.rows(),
                    CvType.CV_8UC1);

            // convert to grayscale
            int colorChannels = (mat.channels() == 3) ? Imgproc.COLOR_BGR2GRAY
                    : ((mat.channels() == 4) ? Imgproc.COLOR_BGRA2GRAY : 1);

            Imgproc.cvtColor(mat, grayMat, colorChannels);

            /* reduce the noise so we avoid false circle detection */
            Imgproc.GaussianBlur(grayMat, grayMat, new Size(3, 3), 3, 3);

            /* create a Mat object to store the circles detected */
            Mat circles = new Mat(mat.cols(),
                    mat.rows(), CvType.CV_8UC1);

            /* find the circle in the image */
            Imgproc.HoughCircles(grayMat, circles,
                    Imgproc.CV_HOUGH_GRADIENT, dp, minDist, param1,
                    param2, minRadius, maxRadius);

            /* get the number of circles detected */
            int numberOfCircles = (circles.rows() == 0) ? 0 : circles.cols();

            // STATISTICAL METHODS TO EXCLUDE FALSE CIRCLES

            // calculate the average radius
            double totalRadius = 0;
            double avgRadius;

            for (int i = 0; i < numberOfCircles; i++) {
                double[] circleCoordinates = circles.get(0, i);

                totalRadius = +(int) circleCoordinates[2];
            }
            avgRadius = totalRadius / numberOfCircles;
            double stdDev = 0;

            // get the standard deviation
            for (int i = 0; i < numberOfCircles; i++) {
                double[] circleCoordinates = circles.get(0, i);

                int radius = (int) circleCoordinates[2];

                stdDev += Math.pow(radius - avgRadius, 2);

            }
            stdDev = Math.sqrt(stdDev / numberOfCircles);

            /* draw the circles found on the image */
            wellLocations = new ArrayList<Point>();
            double tempAvg=0;

            for (int i = 0; i < plateSize && i < numberOfCircles; i++) {

                /* get the circle details, circleCoordinates[0, 1, 2] = (x,y,r)
                 * (x,y) are the coordinates of the circle's center
                 */
                double[] circleCoordinates = circles.get(0, i);


                int x = (int) circleCoordinates[0], y = (int) circleCoordinates[1];

                Point center = new Point(x, y);

                int radius = (int) circleCoordinates[2];

                if (radius < avgRadius + (2 * stdDev) && radius > avgRadius - (2 * stdDev)) // within 2std
                {
                /* circle's outline */
                    Core.circle(mat, center, radius, new Scalar(0,
                            255, 0), 1);

                    /* circle's center outline */
                    Core.rectangle(mat, new Point(x - 5, y - 5),
                            new Point(x + 5, y + 5),
                            new Scalar(0, 128, 255), -1);
                    tempAvg += radius;
                    wellLocations.add(center);
                }
            }
            averageRadius = tempAvg / Math.min(plateSize, numberOfCircles);

        } // detect
        return mat;
    }


    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        // TODO Auto-generated method stub
        mOpenCvCameraView.focusOnTouch(arg1);
        return true;
    }
    @Override
    public void onResume()
    {
        super.onResume();

        mOpenCvCameraView.enableView();

        plateSize = sharedPrefs.getInt("plateSize",96);
        // accumulator value
        dp = Double.parseDouble(sharedPrefs.getString("dp", "1.7"));

        // min and max radii (set these values as you desire)
        minRadius = Integer.parseInt(sharedPrefs.getString("minRad", "20"));
        maxRadius = Integer.parseInt(sharedPrefs.getString("maxRad", "35"));

        // minimum distance between the center coordinates of detected circles in pixels
        minDist = Double.parseDouble(sharedPrefs.getString("minDist", "100"));
        // cannot be less than minimum radius
        if (minDist < minRadius)
            minDist = minRadius;

        // param1 = gradient value used to handle edge detection
        // param2 = Accumulator threshold value for the cv2.CV_HOUGH_GRADIENT method.
        //      The smaller the threshold is, the more circles will be
        //      detected (including false circles).
        //      The larger the threshold is, the more circles will
        //      potentially be returned.
        param1 = Double.parseDouble(sharedPrefs.getString("param1", "70"));
        param2 = Double.parseDouble(sharedPrefs.getString
                ("param2", "50"));


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        else if (id == R.id.action_help) {
            startActivity(new Intent(this, HelpActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
