package uct.cs.blljam010.disvis;

import org.opencv.android.JavaCameraView;
import org.opencv.core.Point;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

public class CustomCameraView extends JavaCameraView implements PictureCallback, Camera.AutoFocusCallback {

    private static final String TAG = "CUSTOM-CAMERA-VIEW";
    private String mPictureFileName;
    private Context context;
    private ArrayList<Point> wellLocations;
    private double averageRadius;


    public CustomCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public void setResolution(Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    public Size getResolution() {

        return mCamera.getParameters().getPreviewSize();

    }

    public void takePicture(final String fileName, Context c, ArrayList<Point> wl, double avgradius) {
        Log.i(TAG, "Taking picture");
        context = c;
        averageRadius = avgradius;
        wellLocations = wl;
        this.mPictureFileName = fileName;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        boolean success = false;
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        File sdCardDirectory = getAlbumStorageDir("DisVis");
        Log.v(TAG, sdCardDirectory.getPath());
        File image = new File(sdCardDirectory, mPictureFileName + ".png");


        // Write the image in a file (in jpeg format)
        try {

            FileOutputStream fos = new FileOutputStream(image);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

            ContentValues cv = new ContentValues();

            // Add metadata
            cv.put(MediaStore.Images.Media.TITLE, mPictureFileName);
            cv.put(MediaStore.Images.Media.DISPLAY_NAME, mPictureFileName);
            cv.put(MediaStore.Images.Media.DESCRIPTION, "Taken with DisVis");
            cv.put(MediaStore.Images.Media.DATE_ADDED, Calendar.getInstance().toString());
            cv.put(MediaStore.Images.Media.DATE_TAKEN, Calendar.getInstance().toString());
            cv.put(MediaStore.Images.Media.DATE_MODIFIED, Calendar.getInstance().toString());
            cv.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
            cv.put(MediaStore.Images.Media.ORIENTATION, 0);

            File parent = image.getParentFile();
            String path = parent.toString().toLowerCase();
            String name = parent.getName().toLowerCase();
            cv.put(MediaStore.Images.ImageColumns.BUCKET_ID, path.hashCode());
            cv.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, name);
            cv.put(MediaStore.Images.Media.SIZE, image.length());

            cv.put(MediaStore.Images.Media.DATA, image.getAbsolutePath());

            Uri result = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cv);
            fos.flush();
            fos.close();
            success = true;

            // Call the image preview activity
            Intent imgPrev = new Intent(context, ImagePreviewActivity.class);
            imgPrev.putExtra("filePath", mPictureFileName + ".png");
            imgPrev.putExtra("averageRadius", averageRadius);
            Log.e("AVGRAD", averageRadius+"");
            imgPrev.putExtra("res_width", getResolution().width);
            imgPrev.putExtra("res_height", getResolution().height);
            imgPrev.putStringArrayListExtra("wellLocations", convertToStrArrayList(wellLocations));
            imgPrev.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(imgPrev);

        } catch (java.io.IOException e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);
        }
        if (success) {
            Log.v(TAG, "IMAGE SAVED SUCCESSFULLY");
        } else {
            Log.e(TAG, "IMAGE NOT SAVED");
        }

    }
    public void setZoom(int n)
    {
        Camera.Parameters parameters = mCamera.getParameters();
        int maxZoom = parameters.getMaxZoom();
        Log.d("TASK", "Max Zoom: "+maxZoom);
        Log.d("TASK", "Current Zoom: " + parameters.getZoom());

        int value = (int) ((n/100D) * maxZoom);
        Log.d("TASK", "setting Zoom: " + value);

        if (parameters.isZoomSupported()) {
            if (value >=0 && value < maxZoom) {
                parameters.setZoom(value);

            } else {
                Log.e("TASK", "Unable to zoom");
            }
        }
        mCamera.setParameters(parameters);
       // mCamera.startPreview();
    }
    private ArrayList<String> convertToStrArrayList(ArrayList<Point> al)
    {
        ArrayList<String> als = new ArrayList<String>();
        for (int i =0; i< al.size();i++)
        {
            double x = al.get(i).x;
            double y = al.get(i).y;

            String combine = x+"#"+y;

            als.add(i, combine);
        }
        return als;
    }
    public void focusOnTouch(MotionEvent event) {
        Rect focusRect = calculateTapArea(event.getRawX(), event.getRawY(), 1f);
        Rect meteringRect = calculateTapArea(event.getRawX(), event.getRawY(), 1.5f);

        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

        if (parameters.getMaxNumFocusAreas() > 0) {
            List<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
            focusAreas.add(new Camera.Area(focusRect, 1000));

            parameters.setFocusAreas(focusAreas);
        }

        if (parameters.getMaxNumMeteringAreas() > 0) {
            List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
            meteringAreas.add(new Camera.Area(meteringRect, 1000));

            parameters.setMeteringAreas(meteringAreas);
        }

        mCamera.setParameters(parameters);
        mCamera.autoFocus(this);
    }
    /**
     * Convert touch position x:y to {@link Camera.Area} position -1000:-1000 to 1000:1000.
     */
    private Rect calculateTapArea(float x, float y, float coefficient) {
        float focusAreaSize = 300;
        int areaSize = Float.valueOf(focusAreaSize * coefficient).intValue();

        int centerX = (int) (x / getResolution().width - 1000);
        int centerY = (int) (y / getResolution().height - 1000);

        int left = clamp(centerX - areaSize / 2, -1000, 1000);
        int top = clamp(centerY - areaSize / 2, -1000, 1000);

        RectF rectF = new RectF(left, top, left + areaSize, top + areaSize);

        return new Rect(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }
    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x <min) {
            return min;
        }
        return x;
    }
    @Override
    public void onAutoFocus(boolean arg0, Camera arg1) {

    }
    public void setFlashMode (Context item, int type){
        Camera.Parameters params = mCamera.getParameters();
        List<String> FlashModes = params.getSupportedFlashModes();

        switch (type){
            case 0:
                if (FlashModes.contains(Camera.Parameters.FLASH_MODE_AUTO))
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                else
                    Toast.makeText(item, "Auto Mode not supported", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                if (FlashModes.contains(Camera.Parameters.FLASH_MODE_OFF))
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                else
                    Toast.makeText(item, "Off Mode not supported", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                if (FlashModes.contains(Camera.Parameters.FLASH_MODE_ON))
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                else
                    Toast.makeText(item, "On Mode not supported", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                if (FlashModes.contains(Camera.Parameters.FLASH_MODE_RED_EYE))
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_RED_EYE);
                else
                    Toast.makeText(item, "Red Eye Mode not supported", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                if (FlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH))
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                else
                    Toast.makeText(item, "Torch Mode not supported", Toast.LENGTH_SHORT).show();
                break;
        }

        mCamera.setParameters(params);
    }
    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }
    public static File getAlbumStorageDir(Context c, String albumName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(c.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }
}