package uct.cs.blljam010.disvis;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.opencv.core.Point;
import org.opencv.core.Size;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/*
    Alot of the computation in this class is simple arithmetic aimed at scaling the [x, y] coordinates to new resolutions.
    The well locations are passed to this class relative to the CameraPreview resolution. They then need to be scaled to the
    resolution of the displayed bitmap. These values need to be scaled again when the orientation of the phone changes.

    Finally the locations need to be scaled to the final bitmap resolution.
*/

public class MyImageView extends ImageView implements View.OnTouchListener{


    private ArrayList<Point> wellLocations; // locations via detection
    private ArrayList<Point> touchLocations; // locations via touch

    private float averageRadius; // average radius from camera preview - can be zero or NaN
    private float avgRad; // average radius scaled to current view

    private Size previewResolution;

    private Paint paint;

    private double canvasWidth;
    private double canvasHeight;

    ArrayList<Point> allLocations;

    public MyImageView(Context context) {
        super(context);
        init(context);
        // TODO Auto-generated constructor stub
    }
    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }
    public void init(Context context)
    {
        paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);

        touchLocations = new ArrayList<Point>();
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        float x = event.getX();
        float y = event.getY();
        int action_id = event.getAction();
        switch (action_id)
        {
            case MotionEvent.ACTION_DOWN:
                // check whether deleting or adding circle
                if (deleteCircle(x,y) && touchLocations != null) {
                    touchLocations.add(new Point(x, y));
                }
                invalidate();
                break;

            case MotionEvent.ACTION_MOVE:
                break;

            case MotionEvent.ACTION_UP:
                break;

            default:
                return true;
        }

        return true;
    }
    private boolean deleteCircle(double x, double y)
    {
        // see if touch point lies within bounds of a circle
        for (int i=0; i<wellLocations.size();i++)
        {
            double d_sqrd = Math.pow(x - (wellLocations.get(i).x*(canvasWidth/previewResolution.width)),2) + Math.pow(y - (wellLocations.get(i).y*(canvasHeight/previewResolution.height)),2);

            if (d_sqrd <= Math.pow(avgRad,2))
            {
                wellLocations.remove(i);
                Log.v("ImagePreview", "Deleting circle");
                return false;
            }
        }
        for (int i=0; i<touchLocations.size();i++)
        {
            double d_sqrd = Math.pow(x - touchLocations.get(i).x,2) + Math.pow(y - touchLocations.get(i).y,2);

            if (d_sqrd <= Math.pow(avgRad,2))
            {
                touchLocations.remove(i);

                return false;
            }
        }
        return true;
    }

    // Draw all the circles
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();

        // if the radius passed is 0
        if (avgRad == 0 || (averageRadius == 0 || Double.isNaN(averageRadius)))
            avgRad = 50;
        else
            avgRad = (float) (averageRadius * Math.sqrt((this.getWidth() * this.getHeight()) / (previewResolution.width * previewResolution.height)));

        if (allLocations != null) {
            for (int i = 0; i < allLocations.size(); i++) {
                float posx = (float) (allLocations.get(i).x * (canvasWidth / this.getDrawable().getIntrinsicWidth()));
                float posy = (float) (allLocations.get(i).y * (canvasHeight / this.getDrawable().getIntrinsicHeight()));

                canvas.drawText((i+1)+"",posx, posy, paint);
                canvas.drawCircle(posx, posy, avgRad, paint);
            }

        } else {
            if (wellLocations != null)
                for (int i = 0; i < wellLocations.size(); i++) {
                    float posx = (float) (wellLocations.get(i).x * (canvasWidth / previewResolution.width));
                    float posy = (float) (wellLocations.get(i).y * (canvasHeight / previewResolution.height));

                    canvas.drawCircle(posx, posy, avgRad, paint);
                }
            if (touchLocations != null)
                for (Point point : touchLocations) {

                    canvas.drawCircle((float) point.x, (float) point.y, avgRad, paint);
                }
        }
    }

    // these values are converted to be relative to the REAL image resolution and then combined into a single arraylist
    public double[][] getWellLocations(Size resolution)
    {
        allLocations = new ArrayList<>();

        double[][] locations = new double[wellLocations.size()+touchLocations.size()][2];
        // scales the detected location
        int index=0;
        for (int i = 0; i < wellLocations.size(); i++)
        {
            // these locations are relative to the camera preview
            double x = wellLocations.get(i).x * (resolution.width/previewResolution.width);
            double y = wellLocations.get(i).y * (resolution.height/previewResolution.height);

            allLocations.add(new Point(x,y));
            index++;
        }

        // scale the on touch locations
        for (int i = 0; i < touchLocations.size(); i++)
        {
            double x = touchLocations.get(i).x * (resolution.width/canvasWidth);
            double y = touchLocations.get(i).y * (resolution.height/canvasHeight);

            allLocations.add(new Point(x,y));
            index++;
        }



        // this will sort the points into their rows
        Collections.sort(allLocations, new PointCompareX());
        Collections.sort(allLocations, new PointCompareY());


        // sort within rows
        int start=0;
        for (int i=1;i < allLocations.size(); i++)
        {
            Point currPoint = allLocations.get(i);
            Point prevPoint = allLocations.get(i-1);

            // if it's in the next row
            if (currPoint.y > prevPoint.y + avgRad*Math.sqrt((resolution.width*resolution.height)/(canvasWidth*canvasHeight)))
            {
                Collections.sort(allLocations.subList(start, i), new PointCompareX());
                start = i;
            }
            // last row
            if (i == allLocations.size()-1)
            {
                Collections.sort(allLocations.subList(start, i+1), new PointCompareX());
            }

        }
        for (int i=0;i < allLocations.size(); i++) {
            locations[i][0] = allLocations.get(i).x;
            locations[i][1] = allLocations.get(i).y;
        }

        return locations;
    }

    // get the radius used
    public int getRadius()
    {
        return (int)avgRad;
    }
    public MyImageView setPreviewResolution(Size res)
    {
        previewResolution = res;

        return this;
    }
    public MyImageView setWellLocations(ArrayList<Point> wl)
    {
        wellLocations = wl;

        return this;
    }

    // set the radius of the circles
    public MyImageView setAverageRadius(double rad)
    {
        averageRadius = (float)rad;

        return this;
    }
    public void setTouchLocations(ArrayList<Point> tl)
    {
        touchLocations = tl;
    }
    // return the touch locations
    public ArrayList<Point> getTouchLocations()
    {
        ArrayList<Point> al = new ArrayList<>();
        // need to get the locations relative to the bitmap
        for (int i=0;i<touchLocations.size();i++) {

            Point p = new Point(touchLocations.get(i).x/canvasWidth, touchLocations.get(i).y/canvasHeight );
            al.add( p );
        }

        return al;
    }

    // COMPARATOR CLASSES - used to sort coordinates by their X and Y coordinates
    class PointCompareX implements Comparator<Point>
    {
        public int compare(Point a, Point b)
        {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x > b.x) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
    class PointCompareY implements Comparator<Point>
    {
        public int compare(Point a, Point b)
        {
            if (a.y < b.y) {
                return -1;
            }
            else if (a.y > b.y) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
}