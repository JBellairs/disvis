package uct.cs.blljam010.disvis;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.audiofx.BassBoost;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;


public class InputActivity extends ActionBarActivity {


    static {
        System.loadLibrary("opencv_java");
        if (!OpenCVLoader.initDebug()){
            Log.e("opencvstatic","OpenCV initialization NOT successful");
        } else
        {
            Log.i("opencvstatic","OpenCV initialization successful");
        }
    }
    private SharedPreferences sharedPrefs;
    private Spinner spinPlateSize;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // set up spinner
        spinPlateSize = (Spinner)findViewById(R.id.editPlateSize);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                                                                            R.array.array_plate_sizes,
                                                                            android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinPlateSize.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    // Hide inputs when checkbox is unchecked
    public void onCheckboxClicked(View view) {

        // Is the view now checked?
        boolean isChecked = ((CheckBox) view).isChecked();

        int visibility = (isChecked) ? View.VISIBLE: View.INVISIBLE;

        findViewById(R.id.lblInitialConcB).setVisibility(visibility);
        findViewById(R.id.editIntialConcB).setVisibility(visibility);
        findViewById(R.id.spaceConcB).setVisibility(visibility);

        findViewById(R.id.lblNameB).setVisibility(visibility);
        findViewById(R.id.editNameB).setVisibility(visibility);
        findViewById(R.id.spaceNameB).setVisibility(visibility);

        findViewById(R.id.lblStartingWellB).setVisibility(visibility);
        findViewById(R.id.editStartingWellB).setVisibility(visibility);
        findViewById(R.id.spaceStartingWellB).setVisibility(visibility);

        //((RelativeLayout)findViewById(R.id.layoutB)).setVisibility(visibility);
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.layoutB);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
        if (!isChecked)
            params.height = 5;
        else
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layout.setLayoutParams(params);
    }

    // The user wants to continue - check that they have entered data in all the fields
    public void cameraButtonPressed(View v)
    {

        String errorMessage="";

        // PLATE ATTRIBUTES
        String nameA = ((EditText)findViewById(R.id.editNameA)).getText().toString();
        String nameB = ((EditText)findViewById(R.id.editNameB)).getText().toString();

        String initCurveConc = ((EditText)findViewById(R.id.editInitCurveConc)).getText().toString();

        // First assign to strings because if the EditText is empty then will throw an exception on Double.ParseDouble - check for null later
        String str_initConcA = ((EditText) findViewById(R.id.editIntialConcA)).getText().toString();
        String str_initConcB = ((EditText)findViewById(R.id.editIntialConcB)).getText().toString();


        String startWellA = ((EditText)findViewById(R.id.editStartingWellA)).getText().toString();
        String startWellB = ((EditText)findViewById(R.id.editStartingWellB)).getText().toString();

        double initConcA;
        double initConcB;

        double concStepA;
        double concStepB;

        // VALIDATE INPUT
        // --------------
        // calibration


        // Drug Names
        if (nameA.equals(""))
        {
            nameA = "Drug A";
            ((EditText)findViewById(R.id.editNameA)).setText("Drug A");
        }
        if (nameB.equals(""))
        {
            nameB = "Drug B";
            ((EditText)findViewById(R.id.editNameB)).setText("Drug B");
        }

        // Initial calibration curve concentration
        if (str_initConcA.equals(""))
        {
            errorMessage += "\nNo starting concentration for the calibration curve";

            // throw error dialog
        }

        // Drug Concentrations
        if (str_initConcA.equals(""))
        {
            errorMessage += "\nNo starting concentration for "+nameA;

            // throw error dialog
        }
        if (str_initConcB.equals(""))
        {
            errorMessage += "\nNo starting concentration for "+nameB;
        }



        // Starting Wells
        if (startWellA.equals(""))
        {
            errorMessage += "\nNo starting well specified for "+nameA;
        }
        if (startWellB.equals(""))
        {
            errorMessage += "\nNo starting well specified for "+nameB;
        }

        // NO ERRORS
        if (errorMessage.equals(""))
        {
            startActivity(new Intent(this, CameraActivity.class));
        }
        else // NO ERRORS
        {
            // Add all values to shared preferences
            sharedPrefs.edit().putInt("plateSize", Integer.parseInt(spinPlateSize.getSelectedItem().toString())).
                    putString("initCurveConc", initCurveConc).
                    putString("drugNameA", nameA).
                    putString("initialConcA", str_initConcA).
                    putString("startingWellA", startWellA).
                    putString("drugNameB", nameB).
                    putString("initialConcB", str_initConcB).
                    putString("startingWellB", startWellB).
                    apply();
                Toast.makeText(getApplicationContext(), sharedPrefs.getString("initCurveConc", "-1"), Toast.LENGTH_SHORT).show();
            /*DBG*///showDialog(this, errorMessage.trim());
            /*DBG*/startActivity(new Intent(this, CameraActivity.class));
        }
    }
    public static void showDialog(Context context, String msg)
    {
        new AlertDialog.Builder(context)
                .setTitle("Invalid Inputs")
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        else if (id == R.id.action_help) {
            startActivity(new Intent(this, HelpActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
