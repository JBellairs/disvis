package uct.cs.blljam010.disvis;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ImagePreviewActivity extends ActionBarActivity{

    private SharedPreferences sharedPrefs;

    // The displayed image
    private MyImageView imgView;
    private Bitmap bmp;
    private String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        // GET IMAGE
        if(getIntent().hasExtra("filePath")) {

            fileName = CustomCameraView.getAlbumStorageDir(getApplicationContext(), "DisVis").getPath()+"/"+getIntent().getStringExtra("filePath");

            MediaScannerConnection.scanFile(this, new String[]{new File(fileName).getPath()}, null, null);
            bmp = BitmapFactory.decodeFile(fileName);

            imgView = (MyImageView) findViewById(R.id.imgPreview);
        }

        // Fetch data from previous activity
        if(getIntent().hasExtra("wellLocations")) {
            imgView.setWellLocations(convertToPointArrayList(getIntent().getStringArrayListExtra("wellLocations")));
        }
        else
            InputActivity.showDialog(this, "Could not locate transfer well locations to activity");

        if(getIntent().hasExtra("averageRadius")) {
            imgView.setAverageRadius(getIntent().getDoubleExtra("averageRadius", 50));
        }
        else
            InputActivity.showDialog(this, "Could not transfer average radius to activity");

        if(getIntent().hasExtra("res_width") && getIntent().hasExtra("res_height")) {
            imgView.setPreviewResolution(new Size(getIntent().getIntExtra("res_width", 0), getIntent().getIntExtra("res_height", 0)));
        }
        else
            InputActivity.showDialog(this, "Could not transfer average radius to activity");

        imgView.setImageBitmap(bmp);

        if(savedInstanceState != null && savedInstanceState.containsKey("touchLocations")) {

            double [][] points = (double[][]) savedInstanceState.getSerializable("touchLocations");
            ArrayList<Point> touchLocations = new ArrayList<>();

            for (double [] point: points)
            {
                touchLocations.add(new Point(point[0],point[1]));
            }
            imgView.setTouchLocations(touchLocations);
        }
        //imgView.normalizePoints();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        double [][] array;
        if (imgView != null) {
            ArrayList<Point> touchLocations = imgView.getTouchLocations();
            array = new double[touchLocations.size()][2];

            for (int i =0; i < touchLocations.size(); i++)
            {
                array[i][0] = touchLocations.get(i).x;
                array[i][1] = touchLocations.get(i).y;
            }

            outState.putSerializable("touchLocations", array);
        }
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onPause()
    {
        super.onPause();



    }
    private ArrayList<Point> convertToPointArrayList(ArrayList<String> als)
    {
        ArrayList<Point> alp = new ArrayList<>();
        for (int i=0;i<als.size();i++)
        {
            String points [] = als.get(i).split("#");

            double x = Double.parseDouble(points[0]);
            double y = Double.parseDouble(points[1]);

            alp.add(i, new Point(x,y));
        }
        return alp;
    }
    // Called when "Accept is pressed"
    public void processCircles(View v)
    {
        int plateSize = sharedPrefs.getInt("plateSize",96);
        double initialConc = Double.parseDouble(sharedPrefs.getString("initCurveConc", "0"));

        // calculate the row length
        int rowLength;
        if (plateSize == 96)
            rowLength = 12;
        else if (plateSize == 24)
            rowLength = 6;
        else if (plateSize == 6)
            rowLength = 3;
        else rowLength = 2;

        // ColourAnalysis module takes coordinates in the form [x1, y1, x2, y2 ...] rather than a 2D array of coords
        //  so need to convert to 1D array

        double [][] wellLocations = imgView.getWellLocations(new Size(bmp.getWidth(), bmp.getHeight()));
        double [] tempConc = new double[rowLength];


        // calibration wells
        int [] control = new int[rowLength*2];

        // the remaining wells
        int [] remainingLocations = new int[(plateSize*2) - (rowLength*2)];
        // populate the curve
        String out="";
        for (int i=0;i<tempConc.length;i++) {
            tempConc[i] = initialConc;
            initialConc/=2;
            out+=tempConc[i];
        }
        Log.v("COLOUR","curve-conc: "+out);
        out="";
        // populate the remaining wells
        for (int i=0,j=0; i < rowLength*2; i+=2, j++)
        {
            control[i] = (int)wellLocations[j][0];
            control[i+1] = (int)wellLocations[j][1];
            out+="["+control[i]+", "+control[i+1]+"]\n";
        }
        Log.v("COLOUR","curve-coords: "+out);
        out="";
        for (int i=0,j=rowLength; i < remainingLocations.length; i+=2, j++)
        {
            remainingLocations[i] = (int)wellLocations[j][0];
            remainingLocations[i+1] = (int)wellLocations[j][1];
            out+="["+remainingLocations[i]+", "+remainingLocations[i+1]+"]\n";
        }
        Log.v("COLOUR","remaining: "+out);
        launchAnalysis(control, tempConc, remainingLocations);


        //InputActivity.showDialog(getApplicationContext(), "File saved: "+colourAnalysis.getFilePath());
    }
    public void launchAnalysis(final int [] control, final double [] tempConc, final int[] remainingLocations) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ImagePreviewActivity.this, "Please Wait", "Calculating well concentrations...", true);
        ringProgressDialog.setCancelable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ColourAnalysis colourAnalysis = new ColourAnalysis(getApplicationContext(), fileName, control, tempConc, remainingLocations, imgView.getRadius());

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error launching Colour Analysis", Toast.LENGTH_LONG).show();
                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_accept)
        {
            processCircles(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}