package uct.cs.blljam010.disvis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.widget.Toast;

import flanagan.interpolation.LinearInterpolation;



public class ColourAnalysis
{
    Context context;
    //private BufferedImage img;
    private Bitmap img;
    private int [] controlCircles;
    private int [] testCircles;
    private float [] finalHues;
    private float [] finalConcs;
    private LinearInterpolation hueInterpolator;
    private LinearInterpolation satInterpolator;
    private LinearInterpolation interpolator;

    private String FILEPATH;
    private final double sin45;
    private int imageWidth;
    private int imageHeight;

    //public static void main(String [] args)
    //{
      	/*
      	//Well location test (same colour)
      	//int [] tempControl = {770, 480, 1038, 478, 1368, 472, 764, 640, 1040, 638, 1375, 632, 754, 862, 1040, 860, 1385, 856};
      	//int [] tempTest = {1040, 696};
   
      	*/
   
      	/*BOX S4
      	//Red Flash
      	int [] tempControl = {474, 232, 592, 230, 706, 228, 822, 224, 936, 220, 1050, 216, 1166, 212, 1278, 208, 1392, 204, 1508, 200, 1620, 195, 1376, 190};
      	int [] tempTest = {606, 690, 720, 685, 838, 680, 952, 678, 1068, 674, 1182, 668, 1298, 665, 1410, 660, 1525, 655, 1640, 650, 1754, 648};
      	double [] tempConc = {12.1737049, 6.125077, 3.064515, 1.573777, 0.777662, 0.411238, 0.192438, 0.10281, 0.056677, 0.031634, 0.014499, 0.007908};
   
   
      	//Yellow Flash
      	int [] tempControl = {478, 350, 596, 348, 710, 344, 826, 340, 940, 334, 1054, 330, 1170, 326, 1282, 324, 1396, 318, 1510, 312, 1624, 310, 1742, 304};
      	int [] tempTest = {610, 806, 728, 800, 840, 796, 956, 794, 1072, 788, 1186, 785, 1300, 780, 1418, 776, 1530, 770, 1642, 766, 1760, 762};
      	double [] tempConc = {63.14488417, 32.51463, 17.14233, 8.918769, 4.361049, 2.243413, 1.164014, 0.596871, 0.320161, 0.176089, 0.109769, 0.068606};
   
   
      	//Green Flash
      	int [] tempControl = {482, 464, 600, 460, 715, 455, 830, 453, 944, 450, 1058, 446, 1174, 442, 1286, 436, 1402, 432, 1515, 428, 1630, 424, 1748, 418};
      	int [] tempTest = {614, 918, 728, 914, 844, 910, 962, 906, 1075, 900, 1190, 896, 1309, 894, 1422, 889, 1537, 884, 1652, 882, 1768, 880};
      	double [] tempConc = {4.83164436, 2.454631, 1.243361, 0.632129, 0.329125, 0.17016, 0.097021, 0.052242, 0.03433, 0.022389, 0.017165, 0.023882};
   
      	//Blue Flash
      	int [] tempControl = {486, 580, 604, 575, 718, 567, 834, 566, 948, 562, 1063, 557, 1178, 554, 1292, 552, 1407, 546, 1520, 544, 1633, 538, 1753, 532};
      	int [] tempTest = {618, 1031, 732, 1029, 849, 1027, 963, 1022, 1079, 1015, 1195, 1013, 1312, 1012, 1428, 1005, 1542, 999, 1659, 996, 1775, 995};
      	double [] tempConc = {4.554761435, 2.297158, 1.153057, 0.594067, 0.303004, 0.154487, 0.086573, 0.050749, 0.030599, 0.023882, 0.019404, 0.020897};
      	*/
   
      	/*
      	//Red Flash No Box
      	int [] tempControl = {535, 417, 615, 416, 695, 416, 777, 414, 855, 416, 935, 415, 1014, 414, 1092, 413, 1171, 412, 1250, 413, 1330, 412, 1408, 410};
      	int [] tempTest = {599, 729, 684, 728, 767, 725, 851, 725, 935, 723, 1017, 722, 1100, 725, 1181, 724, 1264, 722, 1348, 720, 1432, 720};
      	double [] tempConc = {12.1737049, 6.125077, 3.064515, 1.573777, 0.777662, 0.411238, 0.192438, 0.10281, 0.056677, 0.031634, 0.014499, 0.007908};
   
   
      	//Yellow Flash No Box
      	int [] tempControl = {528, 495, 611, 493, 692, 490, 773, 489, 855, 488, 935, 488, 1015, 489, 1094, 489, 1173, 486, 1253, 488, 1333, 487, 1412, 486};
      	int [] tempTest = {592, 811, 678, 810, 765, 809, 850, 808, 935, 806, 1018, 806, 1101, 805, 1184, 805, 1269, 804, 1353, 803, 1436, 803};
      	double [] tempConc = {63.14488417, 32.51463, 17.14233, 8.918769, 4.361049, 2.243413, 1.164014, 0.596871, 0.320161, 0.176089, 0.109769, 0.068606};
   
      	//Green Flash No Box
      	int [] tempControl = {525, 568, 608, 569, 690, 568, 772, 566, 852, 566, 936, 566, 1016, 565, 1095, 563, 1176, 563, 1255, 563, 1336, 561, 1417, 563};
      	int [] tempTest = {588, 898, 675, 895, 761, 894, 848, 893, 934, 891, 1019, 889, 1104, 889, 1188, 888, 1274, 888, 1360, 886, 1446, 885};
      	double [] tempConc = {4.83164436, 2.454631, 1.243361, 0.632129, 0.329125, 0.17016, 0.097021, 0.052242, 0.03433, 0.022389, 0.017165, 0.023882};
   
      	//Blue Flash No Box
      	int [] tempControl = {518, 649, 604, 648, 685, 647, 770, 647, 852, 644, 935, 645, 1016, 643, 1097, 642, 1178, 643, 1260, 640, 1341, 640, 1424, 638};
      	int [] tempTest = {580, 986, 669, 983, 759, 982, 847, 980, 934, 978, 1021, 977, 1107, 977, 1191, 976, 1278, 974, 1362, 974, 1452, 973};
      	double [] tempConc = {4.554761435, 2.297158, 1.153057, 0.594067, 0.303004, 0.154487, 0.086573, 0.050749, 0.030599, 0.023882, 0.019404, 0.020897};
      	*/
   
      	/*
      	//Red Box S5 Lauren low res
      	int [] tempControl = {442, 192, 554, 194, 668, 194, 782, 194, 894, 197, 1004, 198, 1116, 197, 1228, 197, 1338, 195, 1452, 194, 1566, 193};
      	int [] tempTest = {444, 644, 559, 643, 671, 644, 784, 644, 896, 642, 1005, 642, 1120, 643, 1231, 643, 1341, 642, 1454, 641, 1566, 639};
      	double [] tempConc = {87.70008211, 44.56562484, 22.28331043, 11.53971269, 5.864098431, 2.988067238, 1.505237224, 0.751300541, 0.390149053, 0.191120313, 0.10017341};
   
      	//Yellow Box S5 Lauren low res
      	int [] tempControl = {443, 309, 557, 309, 670, 310, 783, 308, 893, 309, 1005, 308, 1116, 306, 1229, 308, 1340, 307, 1450, 305, 1566, 305};
      	int [] tempTest = {447, 754, 559, 754, 675, 755, 785, 756, 896, 752, 1008, 753, 1118, 754, 1232, 752, 1343, 752, 1457, 751, 1567, 753};
      	double [] tempConc = {493.1738, 252.9298, 129.71793, 66.527316, 33.57802, 17.5722617, 9.10400492, 4.59659631, 2.3714778, 1.19831665, 0.61516637};
   
      	//Green Box S5 Lauren low res
      	int [] tempControl = {444, 420, 559, 420, 670, 421, 784, 420, 894, 420, 1005, 421, 1116, 421, 1229, 418, 1339, 420, 1452, 418, 1564, 416};
      	int [] tempTest = {445, 868, 559, 867, 671, 866, 783, 867, 895, 864, 1006, 866, 1120, 865, 1231, 863, 1343, 862, 1458, 864, 1567, 864};
      	double [] tempConc = {26.30268, 13.931568, 7.794516, 3.910691, 2.0680393, 1.067977, 0.548542, 0.29777975, 0.158218814, 0.082094668, 0.050003116};
   
      	//Blue Box S5 Lauren low res
      	int [] tempControl = {444, 532, 558, 532, 671, 531, 783, 532, 895, 532, 1005, 533, 1116, 530, 1228, 531, 1340, 531, 1453, 530, 1565, 527};
      	int [] tempTest = {445, 981, 560, 979, 673, 978, 786, 976, 898, 976, 1009, 976, 1120, 977, 1235, 976, 1344, 975, 1459, 977, 1572, 978};
      	double [] tempConc = {26.30268, 13.89953, 7.8422797, 3.9838303, 2.041172, 1.0403633, 0.551527, 0.27762924, 0.1567262, 0.0835873, 0.05000312};
      	*/
   		/*
      	//Red Box S5 Lauren high res
      	int [] tempControl = {1147, 492, 1443, 496, 1729, 496, 2021, 498, 2313, 498, 2607, 504, 2891, 500, 3183, 508, 3475, 494, 3765, 500, 4057, 496};
      	int [] tempTest = {1151, 1662, 1447, 1656, 1735, 1658, 2031, 1662, 2319, 1654, 2607, 1658, 2895, 1658, 3193, 1660, 3473, 1656, 3761, 1652, 4069, 1651};
      	double [] tempConc = {87.70008211, 44.56562484, 22.28331043, 11.53971269, 5.864098431, 2.988067238, 1.505237224, 0.751300541, 0.390149053, 0.191120313, 0.10017341};
   
      	//Yellow Box S5 Lauren high res
      	int [] tempControl = {1158, 793, 1446, 793, 1740, 793, 2030, 793, 2314, 791, 2606, 797, 2896, 793, 3188, 791, 3474, 789, 3762, 791, 4053, 791};
      	int [] tempTest = {1154, 1955, 1450, 1955, 1736, 1955, 2030, 1951, 2320, 1951, 2607, 1943, 2899, 1953, 3193, 1945, 3479, 1943, 3769, 1941, 4057, 1947};
      	double [] tempConc = {493.1738, 252.9298, 129.71793, 66.527316, 33.57802, 17.5722617, 9.10400492, 4.59659631, 2.3714778, 1.19831665, 0.61516637};
   
      	//Green Box S5 Lauren high res
      	int [] tempControl = {1153, 1084, 1449, 1082, 1735, 1082, 2027, 1084, 2317, 1088, 2605, 1090, 2897, 1084, 3181, 1078, 3469, 1084, 3757, 1082, 4054, 1084};
      	int [] tempTest = {1151, 2243, 1447, 2243, 1739, 2231, 2027, 2237, 2321, 2233, 2604, 2233, 2900, 2231, 3190, 2229, 3476, 2229, 3766, 2229, 4068, 2231};
      	double [] tempConc = {26.30268, 13.931568, 7.794516, 3.910691, 2.0680393, 1.067977, 0.548542, 0.29777975, 0.158218814, 0.082094668, 0.050003116};
   
      	//Blue Box S5 Lauren high res
      	int [] tempControl = {1153, 1369, 1447, 1369, 1741, 1369, 2033, 1368, 2319, 1371, 2609, 1371, 2895, 1373, 3185, 1371, 3471, 1369, 3765, 1367, 4057, 1363};
      	int [] tempTest = {1156, 2537, 1446, 2533, 1740, 2525, 2032, 2525, 2318, 2523, 2609, 2523, 2901, 2521, 3191, 2521, 3479, 2519, 3777, 2525, 4074, 2527};
      	double [] tempConc = {26.30268, 13.89953, 7.8422797, 3.9838303, 2.041172, 1.0403633, 0.551527, 0.27762924, 0.1567262, 0.0835873, 0.05000312};
      	*/


    //  ColourAnalysis temp = new ColourAnalysis("S5 lauren low res.jpg", tempControl, tempConc, tempTest, 40);


    //float [] colour = temp.pixelHSV(562,422);//379,980);//409, 584);
    //temp.printRGB(colour);
    //colour[0] = inverseGamma(colour[0]);
    //colour[1] = inverseGamma(colour[1]);
    //colour[2] = inverseGamma(colour[2]);
    //temp.printRGB(colour);
    //temp.printHSV(colour);


    //System.out.println(temp.wellColour(562,888,20));
    //}

    public ColourAnalysis(Context c, String p, int [] control, double [] controlConc, int [] test, int rad)
    {
        context = c;
        controlCircles = control;
        testCircles = test;
        int r = (int) Math.round(rad*0.8);
        finalHues = new float [(control.length + test.length)/2];
        finalConcs = new float [test.length/2];
        sin45 =  Math.sin(45 / (2 * Math.PI));


        //img = ImageIO.read(new File(p));
        File path = new File(p);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        img = BitmapFactory.decodeFile(path.getAbsolutePath(),bmOptions);

        imageWidth = img.getWidth();
        imageHeight = img.getHeight();

        int param = 0; //0 = hue; 1 = saturation
        float [] hues = new float [control.length/2];
        float [] sats = new float [control.length/2];
        for (int h = 0; h < control.length/2; h++)
        {
            int x = control[h*2];
            int y = control[h*2+1];
            hues[h] = wellColour(x,y,r,0);
            sats[h] = wellColour(x,y,r,1);
        }

        float hueRange [] = standardCurveSetup(controlConc, hues);
        hueInterpolator = interpolator;
        float satRange [] = standardCurveSetup(controlConc, sats);
        satInterpolator = interpolator;
        float range [] = new float [2];

        int huesPos = 0;
        if(Math.abs(hueRange[1]-hueRange[0]) > Math.abs(satRange[1] - satRange[0]))//hueRange[2] > satRange[2] || (hueRange[2] == satRange[2] && Math.abs(hueRange[1] - hueRange[0]) >= Math.abs(satRange[1] - satRange[0])))
        {
            for(int i = 0; i < control.length/2; i++)
            {
                int x = control[i*2];
                int y = control[i*2+1];
                finalHues[huesPos] = hues[huesPos];
                huesPos++;
            }

            param = 0;
            range[0] = hueRange[0];
            range[1] = hueRange[1];
            interpolator = hueInterpolator;
        }
        else
        {
            for(int i = 0; i < control.length/2; i++)
            {
                int x = control[i*2];
                int y = control[i*2+1];
                finalHues[huesPos] = sats[huesPos];
                huesPos++;
            }

            param = 1;
            range[0] = satRange[0];
            range[1] = satRange[1];
            interpolator = satInterpolator;
        }

        for(int j = 0; j < test.length/2; j++)
        {
            int x = test[j*2];
            int y = test[j*2+1];
            finalHues[huesPos] = wellColour(x,y,r,param);
            huesPos++;
        }

        //System.out.println("Param:" + param);

        if (range[0] > range[1])
        {
            float temp = range[1];
            range[1] = range[0];
            range[0] = temp;
        }

        //System.out.println(satInterpolator);

        int concsPos = 0;
        for (int k = control.length/2; k < finalHues.length; k++)
        {
            if (finalHues[k] >= range[0] && finalHues[k] <= range[1])
            {
                finalConcs[concsPos] = (float) interpolator.interpolate((double) finalHues[k]);
            }
            else if (finalHues[k]+360 >= range[0] && finalHues[k]+360 <= range[1])
            {
                finalConcs[concsPos] = (float) interpolator.interpolate((double) (finalHues[k]+360));
            }
            else
            {
                finalConcs[concsPos] = -1;
            }
            concsPos++;
        }
        //System.out.println(range[0] + " " + range[1]);
        //printHues(finalHues);
        //System.out.println();
        printConcs(controlConc);
        //printCurve(range);

    }

    public int [] calcPixelBox(int x, int y, double r)
    {
        int [] temp = new int [4];
        double distance = sin45 * r;

        temp[0] = x-distance < 0 ? (int) 0 : (int) Math.round(x-distance);
        temp[1] = y-distance < 0 ? (int) 0 : (int) Math.round(y-distance);
        temp[2] = x+distance >  imageWidth ? imageWidth : (int) Math.round(x+distance);
        temp[3] = y+distance >  imageHeight ? imageHeight : (int) Math.round(y+distance);

        return temp;
    }

    public float wellColour(int x, int y, int r, int p)
    {
        float finalHue = (float) 0.0;
        int boxDims [] = calcPixelBox(x,y,r);
        float hues [] = new float [(boxDims[2]-boxDims[0]+1)*(boxDims[3]-boxDims[1]+1)];
        int histogram [] = new int [361];

        for (int k = 0; k < 361; k++)
        {
            histogram[k] = 0;
        }

        int huePos = 0;
        for (int i = boxDims[0]; i <= boxDims[2]; i++)
        {
            for (int j = boxDims[1]; j <= boxDims[3]; j++)
            {
                float tempHSV [] = new float [3];
                tempHSV = pixelHSV(i, j);
                float hue = tempHSV[p]/*360*/;

                if(tempHSV[2] > 0.14 && tempHSV[2] < 0.91)
                {
                    histogram[Math.round(hue)] += 1;
                    hues[huePos] = hue;
                    huePos++;
                }

                int modeHue = analyseHistogram(histogram);
                if(modeHue != -1)
                {
                    finalHue = averageColour(modeHue, hues);
                }
            }
        }

        return finalHue;
    }
   
   /*    public static double inverseGamma(double c)
      {
         double colour;
         double nominalCol = c/255.0;
         if(nominalCol <= 0.04045)
         {
            colour = nominalCol/12.92;
         }
         else
         {
            colour = (nominalCol + 0.055)/1.055;
            colour = Math.pow(colour, 2.4);
         }
   
         return colour*255;
      }
   	*/

    public float [] pixelHSV(int x, int y)
    {
        int pixel = img.getPixel(x,y);



        float [] hsv = new float [3];
        Color.RGBToHSV(Color.red(pixel),Color.green(pixel),Color.blue(pixel), hsv);

        return hsv;
    }

    public int analyseHistogram(int [] hist)
    {
        int max = -1;
        int pos = -1;
        for(int i = 0; i < hist.length; i++)
        {
            if(hist[i] > max)
            {
                max = hist[i];
                pos = i;
            }
        }

        return pos;
    }

    public float averageColour(int centre, float [] colours)
    {
        float average = (float) 0.0;
        int count = 0;
        for (int i = 0; i < colours.length; i++)
        {
            if(Math.abs(colours[i] - centre) < 2)
            {
                average += colours[i];
                count++;
            }
        }

        return average/count;
    }

    public float [] standardCurveSetup(double [] concs, float [] values)
    {
        float [] controlHues = new float [concs.length];
        float [] controlConcs = new float [concs.length];
        float [] hueDiffs = new float [controlHues.length-1];

        controlHues[0] = values[0];
        controlConcs[0] = (float) concs[0];
        for (int i = 1; i < controlHues.length; i++)
        {
            controlHues[i] = values[i];
            controlConcs[i] = (float) concs[i];
            hueDiffs[i-1] = controlHues[i] - controlHues[i-1];
        }
        //printHues(controlHues);
      	/*
         if(hueDiffs[0]*hueDiffs[1] < 0)
         {
            float [] tempHues = new float [controlHues.length-1];
            float [] tempConcs = new float [controlConcs.length-1];
            float [] tempHueDiffs = new float [hueDiffs.length-1];
      
            for (int k = 0; k < tempHues.length; k++)
            {
               tempHues[k] = controlHues[k+1];
               tempConcs[k] = controlConcs[k+1];
               if(k+1 < hueDiffs.length)
                  tempHueDiffs[k] = hueDiffs[k+1];
            }
      
            controlHues = tempHues;
            controlConcs = tempConcs;
            hueDiffs = tempHueDiffs;
         }
      	*/
        boolean colourLoop = Math.abs(hueDiffs[0]) > 300 ? true : false;

        int count = controlHues.length;
        int add = 0;
        if(colourLoop)
        {
            if(hueDiffs[0] > 0)
            {
                controlHues[1] += 360;
                add = 360;
            }
            else if(hueDiffs[0] < 0)
            {
                controlHues[0] += 360;
            }
        }

        for (int j = 1; j < hueDiffs.length; j++)
        {
            if(Math.abs(hueDiffs[j]) > 300 && colourLoop == false)
            {
                if(hueDiffs[j] < 0)
                {
                    controlHues[j+1] += 360;
                    add = 360;
                    j++;
                }
                else if(hueDiffs[j] > 0)
                {
                    for (int k = j; k >= 0; k--)
                    {
                        controlHues[k] += 360;
                    }
                }
                colourLoop = true;
            }
            else if((hueDiffs[j] > 0 && hueDiffs[j-1] > 0) || (hueDiffs[j] < 0 && hueDiffs[j-1] < 0))
            {
                controlHues[j+1] += add;
            }
            else if(j > 3)
            {
                count = j+1;
                break;
            }
        }

        double standardHues [];
        double standardConcs [];
        if((controlHues[1]-controlHues[0])*(controlHues[2]-controlHues[1]) < 0)
        {
            standardHues = new double [count-1];
            standardConcs = new double [count-1];
            for(int l = 0; l < standardHues.length; l++)
            {
                standardHues[l] = controlHues[l+1];
                standardConcs[l] = controlConcs[l+1];
            }
        }
        else
        {
            standardHues = new double [count];
            standardConcs = new double [count];
            for(int l = 0; l < standardHues.length; l++)
            {
                standardHues[l] = controlHues[l];
                standardConcs[l] = controlConcs[l];
            }
        }
        //printHues(standardHues);
        if(standardHues.length >= 3)
            interpolator = new LinearInterpolation(standardHues, standardConcs);

        float range [] = {(float) standardHues[0], (float) standardHues[standardHues.length-1], standardHues.length};
        return range;
    }

    public void printRGB(double [] c)
    {
        System.out.println("R: " + c[0] + " G: " + c[1] + " B: " + c[2]);
    }

    public void printHSV(float [] c)
    {
        System.out.println("H: " + c[0] + " S: " + c[1] + " V: " + c[2]);
    }

    public void printHues(float [] hues)
    {
        for (int i = 0; i < controlCircles.length/2; i++)
        {
            System.out.print(hues[i] + " ");
        }
        System.out.println();
        for (int j = controlCircles.length/2; j < finalHues.length; j++)
        {
            System.out.print(hues[j] + " ");
        }
    }

    public void printHues(double [] hues)
    {
        for (int i = 0; i < hues.length; i++)
        {
            System.out.print(hues[i] + " ");
        }
        System.out.println();
    }

    public double roundThreeDecimals(double val)
    {
        val = val*1000;
        val = Math.round(val);
        val = val/1000;
        return val;
    }

    public void printConcs(double [] concs)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        System.out.println();
        String filename = "output_"+dateFormat.format(cal.getTime())+".csv";
        File file = new File(Environment.getExternalStorageDirectory(), filename);

        String outputString = "";
        FileOutputStream outputStream;

        for (int i = 0; i < concs.length; i++)
        {
            outputString += roundThreeDecimals(concs[i]) + ",";
        }
        outputString = outputString.substring(0, outputString.length()-1) + "\n";
        for (int j = 0; j < finalConcs.length; j++)
        {
            outputString += roundThreeDecimals(finalConcs[j]) + ",";
            if(j > 0 && (j+1)%12==0)
                outputString = outputString.substring(0, outputString.length()-1) + "\n";
        }

        try
        {
            outputStream = new FileOutputStream(file);//context.openFileOutput(filename, Context.MODE_WORLD_READABLE);
            outputStream.write(outputString.getBytes());
            outputStream.close();
            FILEPATH = file.getPath();
            Toast.makeText(context, file.getPath(), Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            Toast.makeText(context, "Error saving file", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    public String getFilePath()
    {
        return FILEPATH;
    }
    public void printCurve(float r [])
    {
        for (int i = (int) Math.ceil(r[0]); i < (int) Math.round(r[1]); i++)
        {
            System.out.println(interpolator.interpolate((double) i));
        }
    }
}